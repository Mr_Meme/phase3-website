Luis Martinez -- 1001134879
Thuy Pham     -- 1001531735

For phase 3, our website makes absolutely no use of JavaScript. I am a firm
beliver of functionality over form. The main menu consists of just the four 
buttons, and nothing more. Each method was run through a debugger to rid them
of most bugs; however I would've liked to make them more modular if I had more
time. Methods for manipulating data for a single user will ask for just the ID 
instead of any extra information, as they are not needed when IDs are unique 
keys.

VIEW --- Given a student ID, returns a list of the user's passed milestones.

UPDATE --- Given an instructor ID, changes their tenure from "adjunct" to 
non-tenure.

INSERT --- Given a form consisting of variables for the PHDSTUDENT table, adds 
the given student ID with their information to both PHDSTUDENT and SELFSUPPORT 
by default. Prints SQL error on failure.

DELETE --- Given a student ID, removes the student from both PHDSTUDENT and 
SELFSUPPORT. Prints SQL error on failure.
