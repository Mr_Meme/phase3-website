<html>
  <form action="/doctorate/index.html">
    <input type="Submit" value="Go Back"/>
  </form>
  <head>
    <title>Insert Record</title>
  </head>
  <h1>Phase 3 Database Frontend</h1>
  <h2>Insert Records</h2>
  <p>Enter student information to add to database</p>
  <form action="/doctorate/scripts/insert_backend.php" method="POST">
    Student ID:<br>
    <input type="text" name="StudentId" required><br> 
    First Name:<br>
    <input type="text" name="FName" required><br>
    Last Name: <br>
    <input type="text" name="LName" required><br>
    Starting Semester and Year:<br>
    <select name="StSem">
      <option value="Fall">Fall</option>
      <option value="Winter">Winter</option>
      <option value="Spring">Spring</option>
      <option value="Summer">Summer</option>
    </select>
    <select name="StYear" required>
      <?php
        $min = 1970;
        $max = date('Y');

        foreach(range($max, $min) as $i)
          printf("<option value=\"%s\">%d</option>\n", $i, $i);
      ?>
      </select><br>
    Supervisor ID: <br>
    <input type="text" name="Supervisor" required><br><br>
    <input type="submit">
    <input type="reset">
  </form>
</html>
