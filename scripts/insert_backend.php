<?php
  /*
    Luis Martinez
    Thuy Pham
  */
  echo '<form action="/doctorate/index.html"><input type="submit" value="Go Back"/></form>';
  include_once "dbopen.php";
  $conn = open_database("localhost", "DOCTORATE", "root", "");
  if (!$conn) die('Connection to database failed');

  $studentid = filter_input(INPUT_POST, 'StudentId');
  $fname = filter_input(INPUT_POST, 'FName');
  $lname = filter_input(INPUT_POST, 'LName');
  $stsem = filter_input(INPUT_POST, 'StSem');
  $styear= filter_input(INPUT_POST, 'StYear');
  $superid = filter_input(INPUT_POST, 'Supervisor');

  $query = "INSERT INTO PHDSTUDENT(StudentId, FName, LName, StSem, StYear, Supervisor) VALUES (?, ?, ?, ?, ?, ?)";

  $sql = $conn->prepare($query);

  if (!$sql)
    die('Error: could not prepare query');
  else {
    $sql->bindValue(1, $studentid);
    $sql->bindValue(2, $fname);
    $sql->bindValue(3, $lname);
    $sql->bindValue(4, $stsem);
    $sql->bindValue(5, $styear);
    $sql->bindValue(6, $superid);

    $result = $sql->execute();
    if (!$result)
      die('failed to insert into main table');
    else $sql->closeCursor();

    $query = "INSERT INTO SELFSUPPORT(StudentId) VALUES (?)";
    $sql = $conn->prepare($query);
    $sql->bindValue(1, $studentid);
    $result = $sql->execute();

    if (!$result)
      die('failed to insert into sub table');
    else echo 'Record added successfully to database';
  }
?>