<html>
<!--
  Luis Martinez
  Thuy Pham
-->
  <form action="/doctorate/index.html">
    <input type="submit" value="Go Back"/>
  </form>
</html>

<?php
  $fname = filter_input(INPUT_POST, 'FName');
  $lname = filter_input(INPUT_POST, 'LName');
  $instructorid = filter_input(INPUT_POST, 'InstructorId');

  include_once "dbopen.php";
  try {
    $conn = open_database("localhost", "DOCTORATE", "root", "");
  }
  catch (PDOException $e) {
    die('Error occurred: '.$e->getMessage().'<br>');
  }

  $query = 'UPDATE INSTRUCTOR SET _Type="NTT" WHERE InstructorId = ?';
  $sql = $conn->prepare($query);
  $sql->bindValue(1, $instructorid);
  $result = $sql->execute();

  if (!$result) {
    // $arr = $sql->errorInfo(); # debugging only
    die('An error occurred');
  }

  else echo 'Updated instructor type to non-tenure track.<br>';
?>
