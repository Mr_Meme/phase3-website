<html>
<!-----------
Luis Martinez
Thuy Pham
----------->

  <form action="/doctorate/index.html">
    <input type="submit" value="Go Back"/>
  </form>
  <?php
    include_once "dbopen.php";

    try {
      $conn = open_database("localhost", "DOCTORATE", "root", "");
    }
    catch (PDOException $e) {
      die('Error occurred: '.$e->getMessage().'<br>');
    }

    $studentid = filter_input(INPUT_POST, 'StudentId');
    $sub = 'DELETE FROM SELFSUPPORT WHERE StudentId = ?';
    $main = 'DELETE FROM PHDSTUDENT WHERE StudentId = ?';
    
    $sql = $conn->prepare($sub);
    if (!$sql)
      die('failed to prepare statement');
    else {
      $sql->bindValue(1, $studentid);
      $ret = $sql->execute();

      if (!$ret) {
        die('failed to remove from secondary table: '.$sql->errorInfo()[2]);
      }
      else $sql->closeCursor();

      $sql = $conn->prepare($main);
      $sql->bindValue(1, $studentid);
      $ret = $sql->execute();
      if (!$ret)
        die('failed to remove from primary table: '.$sql->errorInfo()[2]);
      else echo 'successfully removed record';
    }
  ?>
</html>