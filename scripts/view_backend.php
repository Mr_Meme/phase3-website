<html>
  <form action="/doctorate/index.html">
    <input type="submit" value="Go Back"/>
  </form>
</html>

<?php
  include_once "dbopen.php";
  $studentid = filter_input(INPUT_GET, 'StudentId');
  $conn = open_database("localhost", "DOCTORATE", "root", "");

  if (!$conn) die('connection to database failed');

  $query = "SELECT * FROM PHDSTUDENT AS P NATURAL JOIN MILESTONESPASSED AS M WHERE StudentId = ?";
  $sql = $conn->prepare($query);
  $result = $sql->execute(array($studentid));

  if ($result) {
    $rows = $sql->fetchAll(PDO::FETCH_ASSOC);

    if (count($rows) == 0)
      die('No records found for this person');

    echo 'Student ID: '.$rows[0]["StudentId"].'<br>';
    echo 'Supervisor\'s ID: '.$rows[0]["Supervisor"].'<br>';
    echo 'Name: '.$rows[0]["FName"].' '.$rows[0]["LName"].'<br>';
    echo 'Started in the '.$rows[0]["StSem"].' of '.$rows[0]["StYear"].'<br>';
    echo '<br>Milestones passed:<br>';
    foreach($rows as $row)
      echo $row["MId"].' -- passed on '.$row["PassDate"].'<br>';
  }
  else die('Error: Query failed to execute');
?>