<?php declare(strict_types=1);
/*
  Luis Martinez
  Thuy Pham

  Helper function for opening a database in each backend method
*/
  function open_database(string $hostname, string $dbname, string $dbusername, string $dbpassword) {
    return new PDO('mysql:host='.$hostname.';dbname='.$dbname, $dbusername, $dbpassword);
  }
?>